// const listOfCustomers = [];
// const productList = [];

/* function regitration(){
	let email = prompt("Enter Email to Register");
	listOfCustomers.push(new Customer(email,[],[]));

}

function addProduct(){
	let productName = prompt("Enter Product Name:");
	let price = prompt(`Enter price of ${productName}`);
	listOfProducts.push(new Product(productName,price));

}*/

class Customer{
	constructor(email, cart, orders){
		this.email = email;
		this.cart = new Cart();
		this.orders = [];
	}
	login(){
		console.log(`${this.email} has logged in`);
		return this;
	}
	logout(){
		console.log(`${this.email} has logged out`);
		return this;
	}
	checkOut(){
		this.orders.push(this.cart);
		return this;
	}
}

class Product{
	constructor(productName, price){
		this.productName = productName;
		this.price = price;
		this.isActive = true;
	}
	archive(){
		this.isActive = false;
		return this;
	}
	updatePrice(upPrice){
		this.price = upPrice;
		return this;
	}
}

class Cart{
	constructor(prod, qty){
		this.contents = [];
		this.totalAmount = 0;
	}
	addToCart(prod, qty){
		this.contents.push({
			product:prod,
			quantity:qty});
		return this;
	}
	showCartContents(){
		console.log(this.contents);
	}
	updateProductQuantity(prod,qty){
		this.contents.forEach(myCart=>{
			if(myCart.product.productName===prod){
				myCart.quantity=qty;
			}
		})
		return this;
	}
	clearCartContents(){
		this.contents = [];
		return this;
	}
	computeTotal(){
		this.contents.forEach(myCart=>{
			this.totalAmount = this.totalAmount + (myCart.product.price * myCart.quantity);
		})
		return this;
	}
}

let jack = new Customer("jack@mail.com");
let jane = new Customer("jane@mail.com");
let joe = new Customer("joe@mail.com");

let prod001 = new Product("chocolate", 10);
let prod002 = new Product("coke", 50);
let prod003 = new Product("candy", 5);
let prod004 = new Product("chippy", 20);

/*

TEST STATEMENTS

To update product price:
	prod001.updatePrice(15);

To archive product:
	prod001.archive();

Add to Cart:
	joe.cart.addToCart(prod001,5);

Show cart:
	joe.cart.showCartContents();

Update cart quantity:
	joe.cart.updateProductQuantity("chippy",1);

Clear cart:
	joe.cart.clearCartContents();

Compute Cart Total:
	joe.cart.computeTotal();

CheckOut:
	joe.checkOut();	


*/